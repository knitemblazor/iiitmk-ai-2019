# Scheme/Key for University Question paper


## Part A

Each question has been provided with multiple points. Any combination of points may be used to make up
the required number of marks for the question.

Question | Points
---------|-------
1.a      | Decision making entity.
v---v---v| Performs actions to get optimal outcome for itself.
1.b      | Episodic environments are where agents need no memory of past actions to decide right now.
v---v---v| Sequential environments are where agents need memory of past actions.
1.c      | Any 2 of these are needed. Other valid options which are not mentioned here also get 1 mark each..
v---v---v| Human intelligence(HI) has evolved, machine intelligence is designed(MI).
v---v---v| HI is general, MI is specific.
v---v---v| HI degrades gracefuly, MI degrades abrubtly.
v---v---v| HI is subject to fatigue, MI is not.
v---v---v| HI is not easily replicable, MI is easily replicable.
2.a      | It is a search strategy.
v---v---v| Uses heuristic/cost measure to iteratively improve results/solutions.
2.b      | Online search can function without having the entire input available at any given time (as opposed to offline search where all input must be available at all times) .
v---v---v| Adversarial search is planning actions when another agent/opponent/enemy changes the state of the problem in a way which is bad for us continuously.
2.c      | SA is a probabilistic technique for approximating the global optimum of a given function.
v---v---v| SA works in a discrete state space where things like Gradient Descent cannot.
v---v---v| SA finds rough global optima faster than others like GD which find precise local optima faster.
3.a      | Uses the uncertainty capturing mechanism of probability.
v---v---v| Uses the formal argument ability of deductive logic.
v---v---v| Allows for arguments in the face of uncertainty.
3.b      | Knowledge based agents maintain an internal state of knowledge about the environment and use that to operate.
v---v---v| A knowledge based agent is a type of logical agent which applied logic on top of it's knowledge base.
3.c      | A semantic network, or frame network is a knowledge base that represents semantic relations between concepts in a network.
v---v---v| Example usages include NLP,WordNet,existential graphs among others. Please check for other examples online.
4.a      | A layered graph is a graph that has its vertices partitioned into a sequence of layers, and its edges are only permitted to connect vertices between successive layers.
v---v---v| The planning graph is a layered graph in which the layers of vertices form an alternating sequence of literals and operators.
4.b      | **This is a bad question. Please provide 2 marks if any of the following is written.**
v---v---v| Planning involves the representation of actions and world models, reasoning about the effects of actions, and techniques for efficiently searching the space of possible plans.
v---v---v| Decision making can be loosely thought of as different systems for planning. Examples include Markov decision processes.
4.c      | An approach to planning that keeps a partial ordering between actions and only confirms ordering between actions when forced.
v---v---v| The opposite is total order planning.
v---v---v| Relies on principle of least commitment for efficiency.
v---v---v| 4 components. Operators/Actions + partial order + causal links/bindings + open preconditions
5.a      | **Bad question. Please give 2 marks if anything is mentioned**
v---v---v| Allegro graph is a closed source database for storing RDF triplets.
v---v---v| It is related to semantic networks.
5.b      | In memory cache is needed to increase retreival speed of knowldege representation systems.
v---v---v| Some calculations can be done in-memory if the data is available as cache. Otherwise updates to the KR system can take time.
5.c      | Indexing is necessary to return results to queries in sub-linear times.
v---v---v| Inverted indexes for example return results in O(n) where n is the number of terms in the query instead of the corpus being searched upon.
v---v---v| They allow the corpus to grow to extreme sizes without affecting query times a lot.


## Part B

Question | Points
---------|-------
6        | AI agents allows us to frame problem solving in terms of agents and environments.
v---v---v| This leads to many combiations like single agent, multi agent, cooperative agents, competitive agents, stationary environments, incomplete environments, deterministic environments, undeterministic environments.
v---v---v| Simple Reflex Agents         : Ignore the percept history and act only on the basis of the current perception.
v---v---v|                              : The agent function is based on the condition-action rule.
v---v---v|                              : Diagram
v---v---v| Model-Based Reflex Agents    : A model-based agent can handle partially observable environments by use of model about the world.
v---v---v|                              : It works by finding a rule whose condition matches the current situation.
v---v---v|                              : Diagram
v---v---v| Goal-Based Agents            : Take decisions based on how far they are from their goal state.
v---v---v|                              : They usually require search and planning.
v---v---v|                              : Diagram
v---v---v| Utility-Based Agents         : The agents which are built to be used as building blocks.
v---v---v|                              : Utility describes how "happy" the agent is.
v---v---v|                              : Diagram
v---v---v| Learning Agent               : Can learn from its past experiences or it has learning capabilities.
v---v---v|                              : Has parts, learning element, critic, performance element, problem generator.
v---v---v|                              : Diagram
7        | Big data is generally data which is too large/quickly changing to be computed using conventional in-memory algorithms.
v---v---v| Data intensive systems like machine learning algorithms now have a lot of information to absorb during their training phase.
v---v---v| Systems which can learn in a distributed fashion like federated learning/policy gradients can now be used on a massive scale.
v---v---v| Techniques like map-reduce can power conventional algorithms like decision trees to function on large amounts of data.
v---v---v| New algorithms can be developed to take advantage of the large amounts of data available. For example decision streams.
v---v---v| Examples of big data, AI combinations include google keyboard/twitter spam filters/gmail spam filters.
8        | DFS explanation
v---v---v| DFS complexity analysis
v---v---v| BFS explanation
v---v---v| BFS complexity analysis
9        | Decision making problems can be framed as games with players.
v---v---v| Cooperative / non-cooperative games.
v---v---v| Symmetric / asymmetric games.
v---v---v| Zero-sum / non-zero-sum games.
v---v---v| Simultaneous / sequential games.
v---v---v| Perfect information and imperfect information games.
v---v---v| Combinatorial games
v---v---v| Infinitely long games
v---v---v| Discrete and continuous games
v---v---v| Differential games
v---v---v| Metagames
v---v---v| Pooling games
10       | Explain first order logic
v---v---v| Explain formal proof systems
v---v---v| Explain inference system
v---v---v| Maybe mention automated reasoning systems / proof assistants
11       | Mention types of knowledge: declarative knowledge, procedural knowledge, meta-knowledge, heuristic knowledge, and structural knowledge
v---v---v| Simple relational knowledge. Relational database is example.
v---v---v| Inheritable knowledge. Follows class inheritance.
v---v---v| Inferential knowledge. Can derive more facts.
v---v---v| Procedural knowledge.
12       | multiple agents are invloved, most of the time single environment
v---v---v| cooperative/competitive
v---v---v| planning for same goal
v---v---v| plan merging
13       | Allows us to represent a large number of possible transitions efficiently
v---v---v| Describe state space search
v---v---v| Mention uninformed and heuristic search algos
14       | describe what an ontology is
v---v---v| used to limit complexity
v---v---v| usually represented as graphs
v---v---v| examples include word hierarchies
15       | describe web 2 and 3
v---v---v| 2 hilights user generated content
v---v---v| Ajax and javascript play a large role in this phase of the internet
v---v---v| Distributed compute should be a large part of web 3
