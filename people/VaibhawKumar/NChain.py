import gym

env = gym.make("NChain-v0")
env.reset()


def Position(l):
    p = "no_reward"
    q = "small_reward"
    r = "big_reward"
    a = []
    new_position = 0
    for i in range(len(l)):
        i = new_position
        if l[i] == "forward":
            new_position += 1
            a.append("no_reward")
        elif l[i] == "backward":
            new_position -= 1
            a.append("small_reward")
    if new_position == len(l):
        a.append("big_reward")
    if all(a) == p:
        print(p)
    elif q in a and r not in a:
        print(q)
    elif r in a:
        print(r)


env.close()
No_iterations = 4
l = ["forward", "forward", "backward", "forward"]
Position(l)
