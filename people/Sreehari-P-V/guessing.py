import gym
import numpy as np

episodes = 200

# initializing the environment

environment = gym.make("GuessingGame-v0")

environment.reset()


x = 1000
y = -1000

trial = 0

while trial < episodes:
    print("Trial:" + str(trial))
    # geussing based on binary search

    guess = (x + y) / 2

    observation, reward, done, info = environment.step(np.array([guess]))

    if observation == 1:
        print(str(guess) + "is lower than the target")
        y = guess
    if observation == 2:
        print("Correct Value reached:" + str(guess))
        break
    if observation == 3:
        print(str(guess) + " is higher than the target")
        x = guess
        trial = trial + 1


environment.close()
